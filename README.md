## Friends Window -> Item Tracker

Use alt-f to show this window

[Item Tracker](https://gitlab.com/trilibi-eq/UI/raw/master/EQUI_FriendsWnd.xml)

![Rebirth](http://eqc.trilibi.com/ss/ItemTracker.PNG)